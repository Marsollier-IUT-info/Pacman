package pacman;

import sdljava.SDLMain;
import sdljava.SDLException;
import sdljava.video.*;
import sdljava.event.SDLEvent;
import sdljava.event.SDLKeyboardEvent;
import sdljava.event.SDLKey;
import sdljava.event.SDLMouseButtonEvent;
import sdljava.event.SDLMouseMotionEvent;
import sdljava.image.SDLImage;
import sdljava.audio.*;
import sdljava.mixer.*;
import sdljava.ttf.*;


public class Menu_Pause {
	
	public Menu_Pause(SDLSurface screen) throws SDLException, InterruptedException{
//		public Menu_Pause() throws SDLException, InterruptedException{
//		SDLMain.init(SDLMain.SDL_INIT_VIDEO  | SDLMain.SDL_INIT_AUDIO);
//	    SDLSurface screen = SDLVideo.setVideoMode(525, 625, 32, SDLVideo.SDL_DOUBLEBUF | SDLVideo.SDL_HWSURFACE);
//        SDLVideo.wmSetCaption("MP By Axel, Thomas et Adrien Version 0.1", null);
//        
        SDLTTF.init();
        SDLTrueTypeFont monFont = SDLTTF.openFont("Font/PAC-FONT.ttf", 24);
        SDLTrueTypeFont monFontT = SDLTTF.openFont("Font/PAC-FONT.ttf", 40);
        
        SDLSurface titre = monFontT.renderTextSolid("Pause", new SDLColor(255, 242, 0));
        SDLSurface classique = monFont.renderTextShaded("Continuer", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
        SDLSurface mode = monFont.renderTextShaded("Retour", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
        SDLSurface quit = monFont.renderTextShaded("Quitter", new SDLColor(255,0, 0),new SDLColor(143,143,143));
        
        SDLRect posTitre = new SDLRect(171,50);
        titre.blitSurface(screen,posTitre);
        
        SDLRect m1 = new SDLRect(171, 200);
        classique.blitSurface(screen , m1);

        
        SDLRect m2 = new SDLRect(199, 300);
        mode.blitSurface(screen , m2);
        System.out.println(mode.getHeight());
        System.out.println(mode.getWidth());
        
        SDLRect m3 = new SDLRect(189, 400);
        quit.blitSurface(screen , m3);
        System.out.println(quit.getHeight());
        System.out.println(quit.getWidth());
        
        screen.flip();
        
        boolean Running = true;
        
        while (Running) {
            
            SDLEvent event = SDLEvent.waitEvent();
            
            if (event.getType() == SDLEvent.SDL_QUIT)   Running = false;
            if (event.getType() == SDLEvent.SDL_KEYDOWN)
            {
                SDLKeyboardEvent eventK = (SDLKeyboardEvent) event;
                switch (eventK.getSym()) {
                case SDLKey.SDLK_ESCAPE:    Running = false;        break;
                
                case SDLKey.SDLK_RETURN  :
                		//new TestPacman();
                		break;
                }
            }
            
            if(event.getType() == SDLEvent.SDL_MOUSEBUTTONUP){
            	SDLMouseButtonEvent eventM = (SDLMouseButtonEvent) event;
            	if((eventM.getX()>=171 && eventM.getY()>=200) && (eventM.getX()>171 && eventM.getY()<=226) && (eventM.getX()<=360 && eventM.getY()>=200) && (eventM.getX()<=360 && eventM.getY()<=226)){
            		
            		//System.out.println("yay");
            		
            	}
            	if((eventM.getX()>=199 && eventM.getY()>=300) && (eventM.getX()>171 && eventM.getY()<=326) && (eventM.getX()<=337 && eventM.getY()>=300) && (eventM.getX()<=337 && eventM.getY()<=326)){
            		
            		//System.out.println("yay");
            		
            	}
            	
            	if((eventM.getX()>=189 && eventM.getY()>=400) && (eventM.getX()>189 && eventM.getY()<=425) && (eventM.getX()<=324 && eventM.getY()>=400) && (eventM.getX()<=324 && eventM.getY()<=425)){
            		
            		Running = false;
            		
            	}
            	

            }
            
            if(event.getType() == SDLEvent.SDL_MOUSEMOTION){
            	SDLMouseMotionEvent eventM = (SDLMouseMotionEvent) event;
            	screen.fillRect(screen.mapRGB(0, 0, 0));
            	
            	if((eventM.getX()>=171 && eventM.getY()>=200) && (eventM.getX()>171 && eventM.getY()<=226) && (eventM.getX()<=360 && eventM.getY()>=200) && (eventM.getX()<=360 && eventM.getY()<=226)){
            		classique.freeSurface();
            		classique = monFont.renderTextShaded("Continuer", new SDLColor(0, 255, 0),new SDLColor(255,0,255));
            		classique.blitSurface(screen , m1);
            		titre.blitSurface(screen,posTitre);
            		mode.blitSurface(screen , m2);
            		quit.blitSurface(screen , m3);
            		screen.flip();
            		
            	}
            	
            	else if((eventM.getX()>=199 && eventM.getY()>=300) && (eventM.getX()>171 && eventM.getY()<=326) && (eventM.getX()<=337 && eventM.getY()>=300) && (eventM.getX()<=337 && eventM.getY()<=326)){
            		mode.freeSurface();
            		mode = monFont.renderTextShaded("Retour", new SDLColor(0, 255, 0),new SDLColor(255,0,255));
            		classique.blitSurface(screen , m1);
            		titre.blitSurface(screen,posTitre);
            		mode.blitSurface(screen , m2);
            		quit.blitSurface(screen , m3);
            		screen.flip();
            	}
            	
            	else  if((eventM.getX()>=189 && eventM.getY()>=400) && (eventM.getX()>189 && eventM.getY()<=425) && (eventM.getX()<=324 && eventM.getY()>=400) && (eventM.getX()<=324 && eventM.getY()<=425)){
            		quit.freeSurface();
            		quit = monFont.renderTextShaded("Quitter", new SDLColor(255, 0, 0),new SDLColor(255,0,255));
            		classique.blitSurface(screen , m1);
            		titre.blitSurface(screen,posTitre);
            		mode.blitSurface(screen , m2);
            		quit.blitSurface(screen , m3);
            		screen.flip();
            		
            	}
            	else {
            		classique = monFont.renderTextShaded("Continuer", new SDLColor(0, 255, 0),new SDLColor(143,143,143)); 
            		mode = monFont.renderTextShaded("Retour", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
            		quit = monFont.renderTextShaded("Quitter", new SDLColor(255, 0, 0),new SDLColor(143,143,143));
            		classique.blitSurface(screen , m1);
            		titre.blitSurface(screen,posTitre);
            		mode.blitSurface(screen , m2);
            		quit.blitSurface(screen , m3);
            		screen.flip();
            	}
            }
        }
        
        screen.freeSurface();
        SDLTTF.quit();
//        SDLMain.quit();
        
	}


}
