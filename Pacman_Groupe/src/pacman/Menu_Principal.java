package pacman;

import sdljava.SDLMain;
import sdljava.SDLException;
import sdljava.video.*;
import sdljava.x.swig.SWIGTYPE_p__SDL_Joystick;
import sdljava.event.SDLEvent;
import sdljava.event.SDLKeyboardEvent;
import sdljava.event.SDLKey;
import sdljava.event.SDLMouseButtonEvent;
import sdljava.event.SDLMouseMotionEvent;
import sdljava.image.SDLImage;
import sdljava.joystick.*;
import sdljava.audio.*;
import sdljava.mixer.*;
import sdljava.ttf.*;



public class Menu_Principal {
	
	public Menu_Principal() throws SDLException, InterruptedException{
		
		// creation de la fenettre de l'application 
		SDLMain.init(SDLMain.SDL_INIT_VIDEO  | SDLMain.SDL_INIT_AUDIO | SDLMain.SDL_INIT_JOYSTICK);
	    SDLSurface screen = SDLVideo.setVideoMode(525, 625, 32, SDLVideo.SDL_DOUBLEBUF | SDLVideo.SDL_HWSURFACE);
        SDLVideo.wmSetCaption("Pacman By Axel, Thomas et Adrien Version 0.1", null);
        

        
        
        SDLTTF.init();
        SDLTrueTypeFont monFont = SDLTTF.openFont("Font/PAC-FONT.ttf", 24);
        SDLTrueTypeFont monFontT = SDLTTF.openFont("Font/PAC-FONT.ttf", 40);
        
        SDLSurface titre = monFontT.renderTextSolid("Pacman", new SDLColor(255, 242, 0));

        SDLSurface classique = monFont.renderTextShaded("Pacman classique", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
        SDLSurface mode = monFont.renderTextShaded("Multijoueur", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
       // SDLSurface option = monFont.renderTextShaded("option", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
        SDLSurface quit = monFont.renderTextShaded("Quitter", new SDLColor(255, 0, 0),new SDLColor(143,143,143));
        
        SDLRect posTitre = new SDLRect(152,50);
        titre.blitSurface(screen,posTitre);
        System.out.println(titre.getHeight());
        System.out.println(titre.getWidth());
        
        
        SDLRect m1 = new SDLRect(101, 200);
        classique.blitSurface(screen , m1);
        System.out.println(classique.getHeight());
        System.out.println(classique.getWidth());
        
        SDLRect m2 = new SDLRect(150, 300);
        mode.blitSurface(screen , m2);
        System.out.println(mode.getHeight());
        System.out.println(mode.getWidth());
        
        SDLRect m3 = new SDLRect(189, 400);
        quit.blitSurface(screen , m3);
        System.out.println(quit.getHeight());
        System.out.println(quit.getWidth());
        
        screen.flip();
        
        byte choix = 0;
        
        boolean Running = true;
        
        while (Running) {
            
            SDLEvent event = SDLEvent.waitEvent(); 
            
            if (event.getType() == SDLEvent.SDL_QUIT )   Running = false;
            if (event.getType() == SDLEvent.SDL_KEYDOWN)
            {
                SDLKeyboardEvent eventK = (SDLKeyboardEvent) event;
                switch (eventK.getSym()) {
                case SDLKey.SDLK_ESCAPE:    Running = false;        break;
                
                case SDLKey.SDLK_RETURN  :
                		choix = 1;
                		break;
                }
            }
            
            if(event.getType() == SDLEvent.SDL_MOUSEBUTTONUP){
            	SDLMouseButtonEvent eventM = (SDLMouseButtonEvent) event;
            	if((eventM.getX()>=101 && eventM.getY()>=200) && (eventM.getX()>101 && eventM.getY()<=226) && (eventM.getX()<=422 && eventM.getY()>=200) && (eventM.getX()<=422 && eventM.getY()<=226)){
            		
            		choix = 1;
            		
            		Running = false;

            	}
            	
            	if((eventM.getX()>=150 && eventM.getY()>=300) && (eventM.getX()>150 && eventM.getY()<=325) && (eventM.getX()<=373 && eventM.getY()>=300) && (eventM.getX()<=373 && eventM.getY()<=325)){

            		System.out.println("multi");
            		choix = 2;
            		
            		Running = false;
            	}
            	
            	if((eventM.getX()>=189 && eventM.getY()>=400) && (eventM.getX()>189 && eventM.getY()<=425) && (eventM.getX()<=331 && eventM.getY()>=400) && (eventM.getX()<=331 && eventM.getY()<=425)){

            		Running = false;
            	}

            }
            
            if(event.getType() == SDLEvent.SDL_MOUSEMOTION){
            	SDLMouseMotionEvent eventM = (SDLMouseMotionEvent) event;
    
            	
            	if((eventM.getX()>=101 && eventM.getY()>=200) && (eventM.getX()>101 && eventM.getY()<=226) && (eventM.getX()<=422 && eventM.getY()>=200) && (eventM.getX()<=422 && eventM.getY()<=226)){
            		classique.freeSurface();
            		classique = monFont.renderTextShaded("Pacman classique", new SDLColor(0, 255, 0),new SDLColor(255,0,255));
            		classique.blitSurface(screen , m1);
            		titre.blitSurface(screen,posTitre);
            		mode.blitSurface(screen , m2);
            		quit.blitSurface(screen , m3);
            		screen.flip();
            		
            	}
            	
            	else if((eventM.getX()>=150 && eventM.getY()>=300) && (eventM.getX()>150 && eventM.getY()<=325) && (eventM.getX()<=373 && eventM.getY()>=300) && (eventM.getX()<=373 && eventM.getY()<=325)){
            		mode.freeSurface();
            		mode = monFont.renderTextShaded("Multijoueur", new SDLColor(0, 255, 0),new SDLColor(255,0,255));
            		classique.blitSurface(screen , m1);
            		titre.blitSurface(screen,posTitre);
            		mode.blitSurface(screen , m2);
            		quit.blitSurface(screen , m3);
            		screen.flip();
            	}
            	
            	else  if((eventM.getX()>=189 && eventM.getY()>=400) && (eventM.getX()>189 && eventM.getY()<=425) && (eventM.getX()<=331 && eventM.getY()>=400) && (eventM.getX()<=331 && eventM.getY()<=425)){
            		quit.freeSurface();
            		quit = monFont.renderTextShaded("Quitter", new SDLColor(255, 0, 0),new SDLColor(255,0,255));
            		classique.blitSurface(screen , m1);
            		titre.blitSurface(screen,posTitre);
            		mode.blitSurface(screen , m2);
            		quit.blitSurface(screen , m3);
            		screen.flip();
            		
            	}
            	else {
            		classique = monFont.renderTextShaded("Pacman classique", new SDLColor(0, 255, 0),new SDLColor(143,143,143)); 
            		mode = monFont.renderTextShaded("Multijoueur", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
            		quit = monFont.renderTextShaded("Quitter", new SDLColor(255, 0, 0),new SDLColor(143,143,143));
            		classique.blitSurface(screen , m1);
            		titre.blitSurface(screen,posTitre);
            		mode.blitSurface(screen , m2);
            		quit.blitSurface(screen , m3);
            		screen.flip();
            	}
            }
            
      
             
        }
       //screen.fillRect(screen.mapRGB(0, 0, 0));

        titre.freeSurface();
		classique.freeSurface();
		mode.freeSurface();
		quit.freeSurface();
		monFont.closeFont();
		monFontT.closeFont();
		screen.freeSurface();
		SDLTTF.quit();
        SDLMain.quit();
        
        switch(choix)
        {
        	case 1:
        		new TestPacman();
        		break;
        	case 2:
        		new Menu_nbJoueur();
        		break;
        	default:
        		break;
        }
        
	}

}
