package pacman;

import sdljava.SDLException;
import sdljava.SDLMain;
import sdljava.event.SDLEvent;
import sdljava.event.SDLKey;
import sdljava.event.SDLKeyboardEvent;
import sdljava.event.SDLMouseButtonEvent;
import sdljava.event.SDLMouseMotionEvent;
import sdljava.ttf.SDLTTF;
import sdljava.ttf.SDLTrueTypeFont;
import sdljava.video.SDLColor;
import sdljava.video.SDLRect;
import sdljava.video.SDLSurface;
import sdljava.video.SDLVideo;

public class Gagner {
	
	public Gagner() throws SDLException{

		SDLMain.init(SDLMain.SDL_INIT_VIDEO  | SDLMain.SDL_INIT_AUDIO);
		
		SDLSurface ecran = SDLVideo.setVideoMode(525, 625, 32, SDLVideo.SDL_DOUBLEBUF | SDLVideo.SDL_HWSURFACE);
		SDLVideo.wmSetCaption("Pacman By Axel, Thomas et Adrien Version 0.1", null);
		SDLTTF.init();
		SDLTrueTypeFont monFont = SDLTTF.openFont("font.ttf", 30);
		SDLTrueTypeFont monFontT = SDLTTF.openFont("font.ttf", 40);
		
		SDLColor rouge = new SDLColor(255, 0, 0);
		SDLColor jaune = new SDLColor(255, 255, 0);
        //text
		SDLSurface titre = monFontT.renderTextSolid("Pacman",jaune);
			SDLSurface continuer = monFont.renderTextSolid("Continuer", rouge);
	        SDLSurface menu = monFont.renderTextSolid("Menu Principal", rouge);
	        SDLSurface quit = monFont.renderTextSolid("Quitter", rouge);
	        SDLSurface msg = monFont.renderTextSolid("Vous avez gagner !!!", rouge);
        // pos et affichage
		    SDLRect posTitre = new SDLRect(155,50);
	        titre.blitSurface(ecran,posTitre);
	        
	        SDLRect continuerT = new SDLRect(101, 200);
	        continuer.blitSurface(ecran , continuerT);
	       
	        SDLRect menuT = new SDLRect(150, 300);
	        menu.blitSurface(ecran , menuT);
	       
	        SDLRect quitT = new SDLRect(189, 400);
	        quit.blitSurface(ecran , quitT);
	        
	        SDLRect msgT = new SDLRect(70, 130);
	        msg.blitSurface(ecran , msgT);
	        
	        ecran.flip();
		
	     byte choix = 0;
		boolean Running=true;
		while(Running){
			 SDLEvent event = SDLEvent.waitEvent(); 
	            
	            if (event.getType() == SDLEvent.SDL_QUIT )   Running = false;
	            if (event.getType() == SDLEvent.SDL_KEYDOWN)
	            {
	                SDLKeyboardEvent eventK = (SDLKeyboardEvent) event;
	                switch (eventK.getSym()) {
	                case SDLKey.SDLK_ESCAPE:    Running = false;        break;
	                
	                case SDLKey.SDLK_RETURN  :
	                		choix = 1;
	                		Running=false;
	                		break;
	                }
	            }
	            
	            if(event.getType() == SDLEvent.SDL_MOUSEBUTTONUP){
	            	SDLMouseButtonEvent eventM = (SDLMouseButtonEvent) event;
	            	if((eventM.getX()>=101 && eventM.getY()>=200) && (eventM.getX()>101 && eventM.getY()<=226) && (eventM.getX()<=422 && eventM.getY()>=200) && (eventM.getX()<=422 && eventM.getY()<=226)){
	            		Running = false;
	            		choix = 1;
	            		

	            	}
	            	
	            	if((eventM.getX()>=150 && eventM.getY()>=300) && (eventM.getX()>150 && eventM.getY()<=325) && (eventM.getX()<=373 && eventM.getY()>=300) && (eventM.getX()<=373 && eventM.getY()<=325)){

	            		System.out.println("menu principal");
	            		choix = 2;
	            		Running = false;
	            	}
	            	
	            	if((eventM.getX()>=189 && eventM.getY()>=400) && (eventM.getX()>189 && eventM.getY()<=425) && (eventM.getX()<=331 && eventM.getY()>=400) && (eventM.getX()<=331 && eventM.getY()<=425)){

	            		Running = false;
	            	}

	            }
	            
	            if(event.getType() == SDLEvent.SDL_MOUSEMOTION){
	            	SDLMouseMotionEvent eventM = (SDLMouseMotionEvent) event;
	    
	            	
	            	if((eventM.getX()>=101 && eventM.getY()>=200) && (eventM.getX()>101 && eventM.getY()<=226) && (eventM.getX()<=422 && eventM.getY()>=200) && (eventM.getX()<=422 && eventM.getY()<=226)){
	            		continuer.freeSurface();
	            		continuer = monFont.renderTextSolid("Continuer",jaune);
	            		continuer.blitSurface(ecran , continuerT);
	            		titre.blitSurface(ecran,posTitre);
	            		menu.blitSurface(ecran , menuT);
	            		quit.blitSurface(ecran , quitT);
	            		ecran.flip();
	            		
	            	}
	            	
	            	else if((eventM.getX()>=150 && eventM.getY()>=300) && (eventM.getX()>150 && eventM.getY()<=325) && (eventM.getX()<=373 && eventM.getY()>=300) && (eventM.getX()<=373 && eventM.getY()<=325)){
	            		menu.freeSurface();
	            		menu = monFont.renderTextSolid("Menu Principal", jaune);
	            		continuer.blitSurface(ecran , continuerT);
	            		titre.blitSurface(ecran,posTitre);
	            		menu.blitSurface(ecran , menuT);
	            		quit.blitSurface(ecran , quitT);
	            		ecran.flip();
	            	}
	            	
	            	else  if((eventM.getX()>=189 && eventM.getY()>=400) && (eventM.getX()>189 && eventM.getY()<=425) && (eventM.getX()<=331 && eventM.getY()>=400) && (eventM.getX()<=331 && eventM.getY()<=425)){
	            		quit.freeSurface();
	            		quit = monFont.renderTextSolid("Quitter",jaune);
	            		continuer.blitSurface(ecran , continuerT);
	            		titre.blitSurface(ecran,posTitre);
	            		menu.blitSurface(ecran , menuT);
	            		quit.blitSurface(ecran , quitT);
	            		ecran.flip();
	            		
	            	}
	            	else {
	            		continuer = monFont.renderTextSolid("Continuer", rouge); 
	            		menu = monFont.renderTextSolid("Menu Principal", rouge);
	            		quit = monFont.renderTextSolid("Quitter", rouge);
	            		continuer.blitSurface(ecran , continuerT);
	            		titre.blitSurface(ecran,posTitre);
	            		menu.blitSurface(ecran , menuT);
	            		quit.blitSurface(ecran , quitT);
	            		ecran.flip();
	            	}
	            }
	            
	      
	             
	        }
		quit.freeSurface();
		msg.freeSurface();
		menu.freeSurface();
		continuer.freeSurface();
		titre.freeSurface();
		ecran.freeSurface();
		monFont.closeFont();
		SDLTTF.quit();
		SDLMain.quit();
		
		switch(choix)
		{
			case 1:
				new TestPacman();
				break;
			case 2:
			try {
				new Menu_Principal();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			default:
				break;
		}
	}
}
