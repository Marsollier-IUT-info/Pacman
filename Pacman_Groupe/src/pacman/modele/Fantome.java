package pacman.modele;

import java.util.Random;

public class Fantome extends Personnage
{
	private Random aleatoire;
	private int dureeProie;
	private int zone;
	private boolean proie;
	
	public Fantome(Terrain terrain, int abscisse,int ordonnee)
	{
		super(terrain, abscisse, ordonnee);
		aleatoire = new Random();
		dureeProie = 0;
		zone = 100;
		proie = false;
	}
	
	public Fantome(Terrain terrain, int abscisse,int ordonnee, int zone)
	{
		super(terrain, abscisse, ordonnee);
		aleatoire = new Random();
		dureeProie = 0;
		this.zone = zone;
		proie = false;
	}
	
	public int obtenirDureeProie()
	{
		return dureeProie;
	}
	
	public int obtenirZone()
	{
		return zone;
	}
	
	public boolean obtenirProie()
	{
		return proie;
	}
	
	public void definirDureeProie(int dureeProie)
	{
		this.dureeProie = dureeProie;
	}
	
	public void definirZone(int zone)
	{
		this.zone = zone;
	}
	
	public void definirProie(boolean proie)
	{
		this.proie = proie;
	}
	
	public void seDeplacerAleatoirement(Pacman pacman)
	{
		int nombreAleatoire = aleatoire.nextInt(4);
		
		int directionEnvisagee;
		
		switch(nombreAleatoire)
		{
			case 0:
				directionEnvisagee = Direction.GAUCHE;
				break;
				
			case 1:
				directionEnvisagee = Direction.DROITE;
				break;
			
			case 2:
				directionEnvisagee = Direction.HAUT;
				break;
			
			case 3:
				directionEnvisagee = Direction.BAS;
				break;
			
			default:
				directionEnvisagee = Direction.INDETERMINE;
				break;
		}
		
		if(directionEnvisagee != Direction.INDETERMINE && directionEnvisagee != Direction.obtenirDirectionOpposee(obtenirDirection())
		&& (verifierDeplacementPossible(obtenirDirection()) || verifierDeplacementPossible(directionEnvisagee)))
		{
			definirProchaineDirection(directionEnvisagee);
		}
		
		seDeplacer();
	}
	
	public void immiterPacman(Pacman pacman)
	{
		definirProchaineDirection(pacman.obtenirDirection());
		seDeplacer();
	}
	
	public void immiterJoueur(Pacman pacman)
	{
		definirProchaineDirection(pacman.obtenirProchaineDirection());
		seDeplacer();
	}
	
	public void immiterOpposePacman(Pacman pacman)
	{
		definirProchaineDirection(Direction.obtenirDirectionOpposee(pacman.obtenirDirection()));
		seDeplacer();
	}
	
	public void immiterOpposeJoueur(Pacman pacman)
	{
		definirProchaineDirection(Direction.obtenirDirectionOpposee(pacman.obtenirProchaineDirection()));
		seDeplacer();
	}
	
	public void troublerEtHanterPacman(Pacman p)
	{
		if(proie)
			fuirLachement(p);
		
		else
		{
		   int x,y;
		   y=ordonnee-p.ordonnee;
		   x=abscisse-p.abscisse;
		   if((Math.abs(x)+Math.abs(y)) <= zone){
		     if(y < 0)
		      if(x > 0){
		       if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.GAUCHE))
		        definirProchaineDirection(Direction.GAUCHE);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.BAS))
		        definirProchaineDirection(Direction.BAS);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.HAUT))
		        definirProchaineDirection(Direction.HAUT);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.DROITE))
		        definirProchaineDirection(Direction.DROITE);
		      }
		      else if(x < 0){
		       if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.DROITE))
		        definirProchaineDirection(Direction.DROITE);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.BAS))
		        definirProchaineDirection(Direction.BAS);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.HAUT))
		        definirProchaineDirection(Direction.HAUT);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.GAUCHE))
		        definirProchaineDirection(Direction.GAUCHE);
		      }
		      else{
		       if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.BAS))
		        definirProchaineDirection(Direction.BAS);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.GAUCHE))
		        definirProchaineDirection(Direction.GAUCHE);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.DROITE))
		        definirProchaineDirection(Direction.DROITE);
		      }
		    if(y>0)
		      if(x > 0){
		       if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.GAUCHE))
		        definirProchaineDirection(Direction.GAUCHE);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.HAUT))
		        definirProchaineDirection(Direction.HAUT);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.BAS))
		        definirProchaineDirection(Direction.BAS);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.DROITE))
		        definirProchaineDirection(Direction.DROITE);
		      }
		      else if(x < 0){
		       if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.DROITE))
		        definirProchaineDirection(Direction.DROITE);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.HAUT))
		        definirProchaineDirection(Direction.HAUT);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.BAS))
		        definirProchaineDirection(Direction.BAS);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.GAUCHE))
		        definirProchaineDirection(Direction.GAUCHE);
		      }
		      else{
		       if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.HAUT))
		        definirProchaineDirection(Direction.HAUT);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.GAUCHE))
		        definirProchaineDirection(Direction.GAUCHE);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.DROITE))
		        definirProchaineDirection(Direction.DROITE);
		       
		      }      
		     if(y==0)
		      if(x > 0)
		       if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.GAUCHE))
		        definirProchaineDirection(Direction.GAUCHE);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.DROITE))
		        definirProchaineDirection(Direction.DROITE);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.HAUT))
		        definirProchaineDirection(Direction.HAUT);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.BAS))
		        definirProchaineDirection(Direction.BAS);
		      else if(x < 0)
		       if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.DROITE))
		        definirProchaineDirection(Direction.DROITE);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.GAUCHE))
		        definirProchaineDirection(Direction.GAUCHE);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.HAUT))
		        definirProchaineDirection(Direction.HAUT);
		       else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.BAS))
		        definirProchaineDirection(Direction.BAS);
		     seDeplacer();
		   }
		   else{
		    seDeplacerAleatoirement(p);
		   }
		   
		  }
	}
	
	public void fuirLachement(Pacman p)
	{
	  int x,y;
	  y=ordonnee-p.ordonnee;
	  x=abscisse-p.abscisse;
	  if(y==0){
	   if(!(x<=-1)){
	    if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.DROITE))
	     definirProchaineDirection(Direction.DROITE);
	    else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.HAUT))
	     definirProchaineDirection(Direction.HAUT);
	    else
	     definirProchaineDirection(Direction.BAS);
	   }
	   else if(!(x>=1)){
	    if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.GAUCHE))
	     definirProchaineDirection(Direction.GAUCHE);
	    else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.HAUT))
	     definirProchaineDirection(Direction.HAUT);
	    else
	     definirProchaineDirection(Direction.BAS);
	    }
	   }
	  else if(!(y<=-1)){
	   if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.BAS))
	    definirProchaineDirection(Direction.BAS);
	   else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.GAUCHE))
	    definirProchaineDirection(Direction.GAUCHE);
	   else
	    definirProchaineDirection(Direction.DROITE);
	   } 
	  else if(!(y>=1)){
	   if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.HAUT))
	    definirProchaineDirection(Direction.HAUT);
	   else if(obtenirEnMouvement() || verifierDeplacementPossible(Direction.GAUCHE))
	    definirProchaineDirection(Direction.GAUCHE);
	   else
	    definirProchaineDirection(Direction.DROITE); 
	     
	   }
	  seDeplacer();
	}
	
	public void seDeplacer()
	{
		//super.seDeplacer();
		
		for(Pacman pacman : terrain.obtenirPacmans())
		{
			if(obtenirCollisionAvecElement(pacman))
			{
				if(proie)
				{
					definirMort(true);
					definirDureeMort(100);
				}
				
				else
				{
					pacman.definirNombreDeVies(pacman.obtenirNombreDeVies() - 1);
					pacman.definirMort(true);
					System.out.println("Mort !");
				}
			}
		}
		
		if(proie)
		{
			if(dureeProie > 0)
			{
				dureeProie--;
			}
			
			else
			{
				proie = false;
			}
			
			if(obtenirMort())
			{
				dureeProie = 0;
				proie = false;
			}
		}
		
		super.seDeplacer();
	}
}
