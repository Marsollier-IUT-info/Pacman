package pacman.modele;

public class Teleporteur extends Element
{
	private int nouvelleAbscisse;
	private int nouvelleOrdonnee;
	
	public Teleporteur(Terrain terrain, int abscisse, int ordonnee, int nouvelleAbscisse, int nouvelleOrdonnee)
	{
		super(terrain, abscisse, ordonnee);
		this.nouvelleAbscisse= nouvelleAbscisse;
		this.nouvelleOrdonnee= nouvelleOrdonnee;
	}
	
	public int obtenirNouvelleAbscisse()
	{
		return nouvelleAbscisse;
	}
	
	public int obtenirNouvelleOrdonnee()
	{
		return nouvelleOrdonnee;
	}
	
	public void teleporter(Personnage personnage)
	{
		if(obtenirExistance(personnage.obtenirAbscisse(), personnage.obtenirOrdonnee()))
		{
			personnage.definirCoordonnees(nouvelleAbscisse, nouvelleOrdonnee);
		}
	}
}
