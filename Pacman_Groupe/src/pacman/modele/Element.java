package pacman.modele;

public class Element
{
	protected Terrain terrain;
	protected int abscisse;
	protected int ordonnee;
	
	public Element(Terrain terrain, int abscisse, int ordonnee)
	{
		this.terrain = terrain;
		this.abscisse = abscisse;
		this.ordonnee = ordonnee;
	}
	
	public Terrain obtenirTerrain()
	{
		return terrain;
	}
	
	public int obtenirAbscisse()
	{
		return abscisse;
	}
	
	public int obtenirOrdonnee()
	{
		return ordonnee;
	}
	
	public boolean obtenirExistance(int abscisse, int ordonnee)
	{
		return abscisse == this.abscisse && ordonnee == this.ordonnee;
	}
	
	public boolean obtenirCollisionAvecElement(Element element)
	{
		return abscisse >= element.obtenirAbscisse() - 1 && abscisse <= element.obtenirAbscisse() + 1
			&& ordonnee >= element.obtenirOrdonnee() - 1 && ordonnee <= element.obtenirOrdonnee() + 1;
	}
	
	public void definirTerrain(Terrain terrain)
	{
		this.terrain = terrain;
	}
	
	public void definirAbscisse(int abscisse)
	{
		this.abscisse = abscisse;
	}
	
	public void definirOrdonnee(int ordonnee)
	{
		this.ordonnee = ordonnee;
	}
	
	public void definirCoordonnees(int abscisse, int ordonnee)
	{
		definirAbscisse(abscisse);
		definirOrdonnee(ordonnee);
	}
}
