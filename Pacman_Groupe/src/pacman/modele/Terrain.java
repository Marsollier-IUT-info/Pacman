package pacman.modele;

public class Terrain
{
	private int nombreDeChemins;
	private int nombreDeTeleporteurs;
	private int nombreDePacGommes;
	private int nombreDeFantomes;
	private int nombreDePacmans;
	private Chemin[] chemins;
	private Teleporteur[] teleporteurs;
	private PacGomme[] pacGommes;
	private Fantome[] fantomes;
	private Pacman[] pacmans;
	
	public Terrain()
	{
		nombreDeChemins = 0;
		nombreDeTeleporteurs = 0;
		nombreDePacGommes = 0;
		nombreDeFantomes = 0;
		nombreDePacmans = 0;
		chemins = new Chemin[0];
		teleporteurs = new Teleporteur[0];
		pacGommes = new PacGomme[0];
	}
	
	public int obtenirNombreDeChemins()
	{
		return nombreDeChemins;
	}
	
	public int obtenirNombreDeTeleporteurs()
	{
		return nombreDeTeleporteurs;
	}
	
	public int obtenirNombreDePacGommes()
	{
		return nombreDePacGommes;
	}
	
	public int obtenirNombreDeFantomes()
	{
		return nombreDeFantomes;
	}
	
	public int obtenirNombreDePacmans()
	{
		return nombreDePacmans;
	}
	
	public Chemin[] obtenirChemins()
	{
		return chemins;
	}
	
	public Teleporteur[] obtenirTeleporteurs()
	{
		return teleporteurs;
	}
	
	public PacGomme[] obtenirPacGommes()
	{
		return pacGommes;
	}
	
	public Fantome[] obtenirFantomes()
	{
		return fantomes;
	}
	
	public Pacman[] obtenirPacmans()
	{
		return pacmans;
	}
	
	public boolean estChemin(int abscisse, int ordonnee)
	{
		for(int index = 0; index < nombreDeChemins; index++)
		{
			if(chemins[index].obtenirExistance(abscisse, ordonnee))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public boolean estTeleporteur(int abscisse, int ordonnee)
	{
		for(int index = 0; index < nombreDeTeleporteurs; index++)
		{
			if(teleporteurs[index].obtenirExistance(abscisse, ordonnee))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public boolean estPacGomme(int abscisse, int ordonnee)
	{
		for(int index = 0; index < nombreDePacGommes; index++)
		{
			if(pacGommes[index].obtenirExistance(abscisse, ordonnee))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public boolean estFantome(int abscisse, int ordonnee)
	{
		for(int index = 0; index < nombreDeFantomes; index++)
		{
			if(fantomes[index].obtenirExistance(abscisse, ordonnee))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public boolean estPacman(int abscisse, int ordonnee)
	{
		for(int index = 0; index < nombreDePacmans; index++)
		{
			if(pacmans[index].obtenirExistance(abscisse, ordonnee))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public Chemin obtenirChemin(int abscisse, int ordonnee, boolean horizontal)
	{
		for(int index = 0; index < nombreDeChemins; index++)
		{
			if(chemins[index].obtenirExistance(abscisse, ordonnee))
			{
				if(horizontal && chemins[index].obtenirHorizontal()
					|| !horizontal && !chemins[index].obtenirHorizontal())
				{
					return chemins[index];
				}
			}
		}
		
		return null;
	}
	
	public Teleporteur obtenirTeleporteur(int abscisse, int ordonnee)
	{
		for(int index = 0; index < nombreDeTeleporteurs; index++)
		{
			if(teleporteurs[index].obtenirExistance(abscisse, ordonnee))
			{
				return teleporteurs[index];
			}
		}
		
		return null;
	}
	
	public PacGomme obtenirPacGomme(int abscisse, int ordonnee)
	{
		for(int index = 0; index < nombreDePacGommes; index++)
		{
			if(pacGommes[index].obtenirExistance(abscisse, ordonnee))
			{
				return pacGommes[index];
			}
		}
		
		return null;
	}
	
	public Fantome obtenirFantome(int abscisse, int ordonnee)
	{
		for(int index = 0; index < nombreDeFantomes; index++)
		{
			if(fantomes[index].obtenirExistance(abscisse, ordonnee))
			{
				return fantomes[index];
			}
		}
		
		return null;
	}
	
	public Pacman obtenirPacman(int abscisse, int ordonnee)
	{
		for(int index = 0; index < nombreDePacmans; index++)
		{
			if(pacmans[index].obtenirExistance(abscisse, ordonnee))
			{
				return pacmans[index];
			}
		}
		
		return null;
	}
	
	public void ajouterChemin(Chemin chemin)
	{
		Chemin[] anciensChemins = new Chemin[nombreDeChemins];
		
		for(int index = 0; index < nombreDeChemins; index++)
		{
			anciensChemins[index] = chemins[index];
		}
		
		chemins = new Chemin[nombreDeChemins + 1];
		
		for(int index = 0; index < nombreDeChemins; index++)
		{
			chemins[index] = anciensChemins[index];
		}
		
		chemins[nombreDeChemins] = chemin;
		nombreDeChemins++;
	}
	
	public void ajouterTeleporteur(Teleporteur teleporteur)
	{
		Teleporteur[] anciensTeleporteurs = new Teleporteur[nombreDeTeleporteurs];
		
		for(int index = 0; index < nombreDeTeleporteurs; index++)
		{
			anciensTeleporteurs[index] = teleporteurs[index];
		}
		
		teleporteurs = new Teleporteur[nombreDeTeleporteurs + 1];
		
		for(int index = 0; index < nombreDeTeleporteurs; index++)
		{
			teleporteurs[index] = anciensTeleporteurs[index];
		}
		
		teleporteurs[nombreDeTeleporteurs] = teleporteur;
		nombreDeTeleporteurs++;
	}
	
	public void ajouterPacGomme(PacGomme pacGomme)
	{
		PacGomme[] anciennesPacGommes = new PacGomme[nombreDePacGommes];
		
		for(int index = 0; index < nombreDePacGommes; index++)
		{
			anciennesPacGommes[index] = pacGommes[index];
		}
		
		pacGommes = new PacGomme[nombreDePacGommes + 1];
		
		for(int index = 0; index < nombreDePacGommes; index++)
		{
			pacGommes[index] = anciennesPacGommes[index];
		}
		
		pacGommes[nombreDePacGommes] = pacGomme;
		nombreDePacGommes++;
	}
	
	public void ajouterFantome(Fantome fantome)
	{
		Fantome[] anciennesFantomes = new Fantome[nombreDeFantomes];
		
		for(int index = 0; index < nombreDeFantomes; index++)
		{
			anciennesFantomes[index] = fantomes[index];
		}
		
		fantomes = new Fantome[nombreDeFantomes + 1];
		
		for(int index = 0; index < nombreDeFantomes; index++)
		{
			fantomes[index] = anciennesFantomes[index];
		}
		
		fantomes[nombreDeFantomes] = fantome;
		nombreDeFantomes++;
	}
	
	public void ajouterPacman(Pacman pacman)
	{
		Pacman[] anciennesPacmans = new Pacman[nombreDePacmans];
		
		for(int index = 0; index < nombreDePacmans; index++)
		{
			anciennesPacmans[index] = pacmans[index];
		}
		
		pacmans = new Pacman[nombreDePacmans + 1];
		
		for(int index = 0; index < nombreDePacmans; index++)
		{
			pacmans[index] = anciennesPacmans[index];
		}
		
		pacmans[nombreDePacmans] = pacman;
		nombreDePacmans++;
	}
	
	public void retirerPacGomme(PacGomme pacGomme)
	{
		PacGomme[] nouvellesPacGommes = new PacGomme[nombreDePacGommes];
		int indexNouvellesPacGommes = 0;
		
		for(int index = 0; index < nombreDePacGommes; index++)
		{
			if(pacGommes[index] != pacGomme)
			{
				nouvellesPacGommes[indexNouvellesPacGommes] = pacGommes[index];
				indexNouvellesPacGommes++;
			}
		}
		
		pacGommes = new PacGomme[indexNouvellesPacGommes];
		
		for(int index = 0; index < indexNouvellesPacGommes; index++)
		{
			pacGommes[index] = nouvellesPacGommes[index];
		}
		
		nombreDePacGommes = indexNouvellesPacGommes;
	}
	
	public int obtenirLongueurTotaleChemins()
	{
		int longueurTotaleChemins = 0;
		
		for(int index = 0; index < nombreDeChemins; index++)
		{
			longueurTotaleChemins += chemins[index].obtenirLongueur();
		}
		
		return longueurTotaleChemins;
	}
}
