package pacman.modele;

public class PacGomme extends Element
{
	private boolean superPacGomme;
	private int score;
	private int dureeBonus;
	
	public PacGomme(Terrain terrain, int abscisse, int ordonnee)
	{
		super(terrain, abscisse, ordonnee);
		superPacGomme = false;
		score = 10;
		dureeBonus = 0;
	}
	
	public PacGomme(Terrain terrain, int abscisse, int ordonnee, boolean superPacGomme)
	{
		super(terrain, abscisse, ordonnee);
		this.superPacGomme = superPacGomme;
		
		if(superPacGomme)
		{
			score = 100;
			dureeBonus = 1000;
		}
		
		else
		{
			score = 10;
			dureeBonus = 0;
		}
	}
	
	public PacGomme(Terrain terrain, int abscisse, int ordonnee, boolean superPacGomme, int score)
	{
		super(terrain, abscisse, ordonnee);
		this.superPacGomme = superPacGomme;
		this.score = score;
		
		if(superPacGomme)
		{
			dureeBonus = 1000;
		}
		
		else
		{
			dureeBonus = 0;
		}
	}
	
	public PacGomme(Terrain terrain, int abscisse, int ordonnee, boolean superPacGomme, int score, int dureeBonus)
	{
		super(terrain, abscisse, ordonnee);
		this.superPacGomme = superPacGomme;
		this.score = score;
		this.dureeBonus = dureeBonus;
	}
	
	public boolean obtenirSuperPacGomme()
	{
		return superPacGomme;
	}
	
	public int obtenirScore()
	{
		return score;
	}
	
	public int obtenirDureeBonus()
	{
		return dureeBonus;
	}
	
	public void definirSuperPacGomme(boolean superPacGomme)
	{
		this.superPacGomme = superPacGomme;
	}
	
	public void definirScore(int score)
	{
		this.score = score;
	}
	
	public void definirDureeBonus(int dureeBonus)
	{
		this.dureeBonus = dureeBonus;
	}
}
