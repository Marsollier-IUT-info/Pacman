package pacman.modele;

public final class Direction
{
	public final static int INDETERMINE = 0;
	public final static int GAUCHE = 1;
	public final static int DROITE = 2;
	public final static int HAUT = 3;
	public final static int BAS = 4;
	
	private Direction(){}
	
	public final static int obtenirDirectionOpposee(int direction)
	{
		switch(direction)
		{
			case GAUCHE:
				return DROITE;
			
			case DROITE:
				return GAUCHE;
			
			case HAUT:
				return BAS;
			
			case BAS:
				return HAUT;
			
			default:
				return INDETERMINE;
		}
	}
	
	public final static int obtenirDirectionAGauche(int direction)
	{
		switch(direction)
		{
			case GAUCHE:
				return BAS;
			
			case DROITE:
				return HAUT;
			
			case HAUT:
				return GAUCHE;
			
			case BAS:
				return DROITE;
			
			default:
				return INDETERMINE;
		}
	}
	
	public final static int obtenirDirectionADroite(int direction)
	{
		switch(direction)
		{
			case GAUCHE:
				return HAUT;
			
			case DROITE:
				return BAS;
			
			case HAUT:
				return DROITE;
			
			case BAS:
				return GAUCHE;
			
			default:
				return INDETERMINE;
		}
	}
	
	public final static String obtenirNomDirection(int direction)
	{
		switch(direction)
		{
			case GAUCHE:
				return "GAUCHE";
			
			case DROITE:
				return "DROITE";
			
			case HAUT:
				return "HAUT";
			
			case BAS:
				return "BAS";
			
			default:
				return "INDETERMINE";
		}
	}
}
