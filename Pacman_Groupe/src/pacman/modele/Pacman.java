package pacman.modele;

public class Pacman extends Personnage
{
	private int nombreDeVies;
	private int score;
	private boolean restaurationPacGomme;
	private boolean restaurationSuperPacGomme;
	
	public Pacman(Terrain terrain, int abscisse, int ordonnee)
	{
		super(terrain, abscisse, ordonnee);
		nombreDeVies = 3;
		score = 0;
		restaurationPacGomme = false;
		restaurationSuperPacGomme = false;
	}
	
	public Pacman(Terrain terrain, int abscisse, int ordonnee, int nombreDeVies)
	{
		super(terrain, abscisse, ordonnee);
		this.nombreDeVies = nombreDeVies;
		score = 0;
		restaurationPacGomme = false;
		restaurationSuperPacGomme = false;
	}
	
	public Pacman(Terrain terrain, int abscisse, int ordonnee, int nombreDeVies, int vitesse)
	{
		super(terrain, abscisse, ordonnee, vitesse);
		this.nombreDeVies = nombreDeVies;
		score = 0;
		restaurationPacGomme = false;
		restaurationSuperPacGomme = false;
	}
	
	public int obtenirNombreDeVies()
	{
		return nombreDeVies;
	}
	
	public int obtenirScore()
	{
		return score;
	}
	
	public boolean obtenirRestaurationPacGomme()
	{
		return restaurationPacGomme;
	}
	
	public boolean obtenirRestaurationSuperPacGomme()
	{
		return restaurationSuperPacGomme;
	}
	
	public void definirNombreDeVies(int nombreDeVies)
	{
		this.nombreDeVies = nombreDeVies;
	}
	
	public void definirScore(int score)
	{
		this.score = score;
	}
	
	public void definirRestaurationPacGomme(boolean restaurationPacGomme)
	{
		this.restaurationPacGomme = restaurationPacGomme;
	}
	
	public void definirRestaurationSuperPacGomme(boolean restaurationSuperPacGomme)
	{
		this.restaurationSuperPacGomme = restaurationSuperPacGomme;
	}
	
	public void seDeplacer()
	{
		if(terrain.estPacGomme(abscisse, ordonnee))
		{
			if(terrain.obtenirPacGomme(abscisse, ordonnee).obtenirSuperPacGomme())
			{
				restaurationSuperPacGomme = true;
				
				for(Fantome fantome : terrain.obtenirFantomes())
				{
					fantome.definirProie(true);
					fantome.definirDureeProie(terrain.obtenirPacGomme(abscisse, ordonnee).obtenirDureeBonus());
				}
			}
			
			score += terrain.obtenirPacGomme(abscisse, ordonnee).obtenirScore();
			restaurationPacGomme = true;
			terrain.retirerPacGomme(terrain.obtenirPacGomme(abscisse, ordonnee));
		}
		
		else
		{
			if(restaurationPacGomme)
			{
				restaurationPacGomme = false;
				
				if(restaurationSuperPacGomme)
				{
					restaurationSuperPacGomme = false;
				}
			}
		}
		
		super.seDeplacer();
	}
}
