package pacman.modele;

public class Chemin extends Element
{
	private int longueur;
	private boolean horizontal;
	
	public Chemin(Terrain terrain, int abscisse, int ordonnee, int longueur, boolean horizontal)
	{
		super(terrain, abscisse, ordonnee);
		this.longueur = longueur;
		this.horizontal = horizontal;
	}
	
	public Chemin(Terrain terrain, int abscisse, int ordonnee, int longueur, boolean horizontal, int intervallePacGommes)
	{
		super(terrain, abscisse, ordonnee);
		this.longueur = longueur;
		this.horizontal = horizontal;
		genererPacGommes(intervallePacGommes);
	}
	
	public int obtenirLongueur()
	{
		return longueur;
	}
	
	public boolean obtenirHorizontal()
	{
		return horizontal;
	}
	
	public boolean obtenirExistance(int abscisse, int ordonnee)
	{
		return horizontal && ordonnee == this.ordonnee && abscisse >= this.abscisse && abscisse <= this.abscisse+ longueur
			|| !horizontal && abscisse == this.abscisse && ordonnee >= this.ordonnee && ordonnee <= this.ordonnee+ longueur;
	}
	
	public void genererPacGommes(int intervallePacGommes)
	{
		if(horizontal)
		{
			for(int index = abscisse; index <= abscisse + longueur; index += intervallePacGommes)
			{
				if(!terrain.estPacGomme(index, ordonnee))
				{
					terrain.ajouterPacGomme(new PacGomme(terrain, index, ordonnee));
				}
			}
		}
		
		else
		{
			for(int index = ordonnee; index <= ordonnee + longueur; index += intervallePacGommes)
			{
				if(!terrain.estPacGomme(abscisse, index))
				{
					terrain.ajouterPacGomme(new PacGomme(terrain, abscisse, index));
				}
			}
		}
	}
}
