package pacman.modele;

public class Personnage extends Element
{
	private double vitesse;
	private int direction;
	private int prochaineDirection;
	private int ancienneAbscisse;
	private int ancienneOrdonnee;
	private int dureeMort;
	private boolean enMouvement;
	private boolean mort;
	
	public Personnage(Terrain terrain, int abscisse, int ordonnee)
	{
		super(terrain, abscisse, ordonnee);
		vitesse = 1;
		direction = Direction.INDETERMINE;
		prochaineDirection = Direction.INDETERMINE;
		ancienneAbscisse = abscisse;
		ancienneOrdonnee = ordonnee;
		dureeMort = 0;
		enMouvement = false;
		mort = false;
	}
	
	public Personnage(Terrain terrain, int abscisse, int ordonnee, double vitesse)
	{
		super(terrain, abscisse, ordonnee);
		this.vitesse = vitesse;
		direction = Direction.INDETERMINE;
		prochaineDirection = Direction.INDETERMINE;
		ancienneAbscisse = abscisse;
		ancienneOrdonnee = ordonnee;
		dureeMort = 0;
		enMouvement = false;
		mort = false;
	}
	
	public double obtenirVitesse()
	{
		return vitesse;
	}
	
	public int obtenirDirection()
	{
		return direction;
	}
	
	public int obtenirProchaineDirection()
	{
		return prochaineDirection;
	}
	
	public int obtenirAncienneAbscisse()
	{
		return ancienneAbscisse;
	}
	
	public int obtenirAncienneOrdonnee()
	{
		return ancienneOrdonnee;
	}
	
	public int obtenirDureeMort()
	{
		return dureeMort;
	}
	
	public boolean obtenirEnMouvement()
	{
		return enMouvement;
	}
	
	public boolean obtenirMort()
	{
		return mort;
	}
	
	public void definirVitesse(int vitesse)
	{
		this.vitesse = vitesse;
	}
	
	public void definirProchaineDirection(int direction)
	{
		prochaineDirection = direction;
	}
	
	public void definirAncienneAbscisse(int abscisse)
	{
		ancienneAbscisse = abscisse;
	}
	
	public void definirAncienneOrdonnee(int ordonnee)
	{
		ancienneOrdonnee = ordonnee;
	}
	
	public void definirAnciennesCoordonnees(int abscisse, int ordonnee)
	{
		ancienneAbscisse = abscisse;
		ancienneOrdonnee = ordonnee;
	}
	
	public void definirDureeMort(int dureeMort)
	{
		this.dureeMort = dureeMort;
	}
	
	public void definirMort(boolean mort)
	{
		this.mort = mort;
	}
	
	public boolean verifierDeplacementPossible(int direction)
	{
		switch(direction)
		{
			case Direction.GAUCHE:
				
				if(terrain.estChemin(abscisse - 1, ordonnee))
				{
					return true;
				}
				
				return false;
				
			case Direction.DROITE:
				
				if(terrain.estChemin(abscisse + 1, ordonnee))
				{
					return true;
				}
				
				return false;
				
			case Direction.HAUT:
				
				if(terrain.estChemin(abscisse, ordonnee - 1))
				{
					return true;
				}
				
				return false;
				
			case Direction.BAS:
				
				if(terrain.estChemin(abscisse, ordonnee + 1))
				{
					return true;
				}
				
				return false;
			
			default:
				return false;
		}
	}
	
	public void seDeplacer()
	{
		if(mort)
		{
			ancienneAbscisse = abscisse;
			ancienneOrdonnee = ordonnee;
			
			if(!enMouvement)
			{
				enMouvement = true;
			}
			
			if(dureeMort > 0)
			{
				dureeMort--;
			}
			
			else
			{
				mort = false;
			}
		}
		
		else
		{
			if(enMouvement)
			{
				ancienneAbscisse = abscisse;
				ancienneOrdonnee = ordonnee;
			}
			
			if(direction != prochaineDirection)
			{
				switch(prochaineDirection)
				{	
					case Direction.GAUCHE:
						
						if(verifierDeplacementPossible(Direction.GAUCHE))
						{
							direction = prochaineDirection;
						}
						
						break;
						
					case Direction.DROITE:
						
						if(verifierDeplacementPossible(Direction.DROITE))
						{
							direction = prochaineDirection;
						}
						
						break;
						
					case Direction.HAUT:
						
						if(verifierDeplacementPossible(Direction.HAUT))
						{
							direction = prochaineDirection;
						}
						
						break;
						
					case Direction.BAS:
						
						if(verifierDeplacementPossible(Direction.BAS))
						{
							direction = prochaineDirection;
						}
						
						break;
						
					default:
						direction = prochaineDirection;
						break;
				}
			}
			
			switch(direction)
			{	
				case Direction.GAUCHE:
					
					if(verifierDeplacementPossible(Direction.GAUCHE))
					{
						abscisse--;
					}
					
					break;
					
				case Direction.DROITE:
					
					if(verifierDeplacementPossible(Direction.DROITE))
					{
						abscisse++;
					}
					
					break;
					
				case Direction.HAUT:
					
					if(verifierDeplacementPossible(Direction.HAUT))
					{
						ordonnee--;
					}
					
					break;
					
				case Direction.BAS:
					
					if(verifierDeplacementPossible(Direction.BAS))
					{
						ordonnee++;
					}
					
					break;
					
				default:
					break;
			}
			
			if(terrain.estTeleporteur(abscisse, ordonnee))
			{
				terrain.obtenirTeleporteur(abscisse, ordonnee).teleporter(this);
			}
			
			if(enMouvement && abscisse == ancienneAbscisse && ordonnee == ancienneOrdonnee)
			{
				enMouvement = false;
			}
			
			if(!enMouvement && (abscisse != ancienneAbscisse || ordonnee != ancienneOrdonnee))
			{
				enMouvement = true;
			}
		}
	}
}
