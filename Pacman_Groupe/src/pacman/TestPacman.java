package pacman;

import sdljava.SDLMain;

import pacman.modele.Direction;
import pacman.modele.Fantome;
import pacman.modele.Pacman;
import pacman.modele.Terrain;
import sdljava.SDLException;
import sdljava.video.*;
import sdljava.audio.SDLAudio;
import sdljava.event.SDLEvent;
import sdljava.event.SDLKeyboardEvent;
import sdljava.event.SDLMouseButtonEvent;
import sdljava.event.SDLKey;
import sdljava.image.SDLImage;
import sdljava.mixer.MixChunk;
//import sdljava.mixer.MixChunk;
import sdljava.mixer.MixMusic;
import sdljava.mixer.SDLMixer;
import sdljava.ttf.SDLTTF;
import sdljava.ttf.SDLTrueTypeFont;

public class TestPacman
{
	public TestPacman() throws SDLException{

		SDLMain.init(SDLMain.SDL_INIT_VIDEO  | SDLMain.SDL_INIT_AUDIO);
		SDLTTF.init();
		
		SDLVideo.wmSetCaption("Pacman par Axel, Thomas et Adrien", null);
		
		SDLMixer.openAudio(44100, SDLAudio.AUDIO_S16SYS, 2 , 1024);
		
		SDLSurface ecran = SDLVideo.setVideoMode(525, 625, 32, SDLVideo.SDL_DOUBLEBUF | SDLVideo.SDL_HWSURFACE);
		
		// Chargement de toutes les images.
		SDLSurface imageFond = SDLImage.load("Sprite/background.bmp");
		SDLSurface imageVide = SDLImage.load("Sprite/vide.png");
		SDLSurface imagePacman = SDLImage.load("Sprite/pacman.png");
		SDLSurface imagePetitePacGomme = SDLImage.load("Sprite/petite-pac-gomme.png");
		SDLSurface imageSuperPacGomme = SDLImage.load("Sprite/super-pac-gomme.png");
		SDLSurface imageSuperPacGomme2 = SDLImage.load("Sprite/super-pac-gomme-2.png");
		SDLSurface imagePacmanGauche = SDLImage.load("Sprite/pacman-L.png");
		SDLSurface imagePacmanDroite = SDLImage.load("Sprite/pacman-R.png");
		SDLSurface imagePacmanHaut = SDLImage.load("Sprite/pacman-U.png");
		SDLSurface imagePacmanBas = SDLImage.load("Sprite/pacman-D.png");
		SDLSurface imagePacmanGauche2 = SDLImage.load("Sprite/pacman-L-2.png");
		SDLSurface imagePacmanDroite2 = SDLImage.load("Sprite/pacman-R-2.png");
		SDLSurface imagePacmanHaut2 = SDLImage.load("Sprite/pacman-U-2.png");
		SDLSurface imagePacmanBas2 = SDLImage.load("Sprite/pacman-D-2.png");
		SDLSurface imageFantome = SDLImage.load("Sprite/cyanghost.png");
		SDLSurface imageFantome2 = SDLImage.load("Sprite/pinkghost.png");
		SDLSurface imageFantome3 = SDLImage.load("Sprite/redghost.png");
		SDLSurface imageFantome4 = SDLImage.load("Sprite/yellowghost.png");
		SDLSurface imageFantomeProie = SDLImage.load("Sprite/vulnghost.png");
		SDLSurface imageVie = SDLImage.load("Sprite/pacman-R.png");
		SDLSurface imagePacmanJ2 = SDLImage.load("Sprite/pacman-J2.png");
		SDLSurface imageVieJ2 = SDLImage.load("Sprite/pacman-J2.png");
		
		MixMusic musique = SDLMixer.loadMUS("Sound/Theme.ogg");
		
		MixChunk sonRestaurationPacGomme = SDLMixer.loadWAV("Sound/Chomping.ogg");
		
		// Creation du terrain, avec chemins, teleporteurs et pac-gommes.
		Terrain terrain = GenerationTerrain.genererTerrain();
		
		// Creation du pacman.
		Pacman pacman = new Pacman(terrain, 245, 403);
		terrain.ajouterPacman(pacman);
		
		Pacman pacmanJ2 = null;
		
		if(Parametres.MULTIJOUEUR)
		{
			pacmanJ2 = new Pacman(terrain, 245, 403);
			terrain.ajouterPacman(pacmanJ2);
		}
		
		// Creation des fantomes.
		Fantome fantome = new Fantome(terrain, 245, 199);
		terrain.ajouterFantome(fantome);
		Fantome fantome2 = new Fantome(terrain, 245, 199);
		terrain.ajouterFantome(fantome2);
		Fantome fantome3 = new Fantome(terrain, 245, 199);
		terrain.ajouterFantome(fantome3);
		Fantome fantome4 = new Fantome(terrain, 245, 199);
		terrain.ajouterFantome(fantome4);
		
		System.out.println("Test : " + terrain.obtenirNombreDePacGommes());
		
		// mise en place du fond (avec les murs).
		SDLRect positionImageFond = new SDLRect(0, 0);
		imageFond.blitSurface(ecran, positionImageFond);

		// Affichage du pacman.
		SDLRect positionImagePacman = new SDLRect(pacman.obtenirAbscisse(), pacman.obtenirOrdonnee());
		imagePacman.blitSurface(ecran, positionImagePacman);
		SDLRect positionImageVidePacman = new SDLRect(pacman.obtenirAbscisse(), pacman.obtenirOrdonnee());
		
		SDLRect positionImagePacmanJ2 = null;
		SDLRect positionImageVidePacmanJ2 = null;
		
		if(Parametres.MULTIJOUEUR)
		{
			positionImagePacmanJ2 = new SDLRect(pacmanJ2.obtenirAbscisse(), pacmanJ2.obtenirOrdonnee());
			imagePacmanJ2.blitSurface(ecran, positionImagePacmanJ2);
			positionImageVidePacmanJ2 = new SDLRect(pacmanJ2.obtenirAbscisse(), pacmanJ2.obtenirOrdonnee());
		}
		
		// Affichage des fantomes.
		SDLRect positionImageFantome = new SDLRect(fantome.obtenirAbscisse(), fantome.obtenirOrdonnee());
		imageFantome.blitSurface(ecran, positionImageFantome);
		SDLRect positionImageVideFantome = new SDLRect(fantome.obtenirAbscisse(), fantome.obtenirOrdonnee());
		
		SDLRect positionImageFantome2 = new SDLRect(fantome2.obtenirAbscisse(), fantome2.obtenirOrdonnee());
		imageFantome2.blitSurface(ecran, positionImageFantome2);
		SDLRect positionImageVideFantome2 = new SDLRect(fantome2.obtenirAbscisse(), fantome2.obtenirOrdonnee());
		
		SDLRect positionImageFantome3 = new SDLRect(fantome3.obtenirAbscisse(), fantome3.obtenirOrdonnee());
		imageFantome3.blitSurface(ecran, positionImageFantome3);
		SDLRect positionImageVideFantome3 = new SDLRect(fantome3.obtenirAbscisse(), fantome3.obtenirOrdonnee());
		
		SDLRect positionImageFantome4 = new SDLRect(fantome4.obtenirAbscisse(), fantome4.obtenirOrdonnee());
		imageFantome4.blitSurface(ecran, positionImageFantome4);
		SDLRect positionImageVideFantome4 = new SDLRect(fantome4.obtenirAbscisse(), fantome4.obtenirOrdonnee());
		
		// Affichage des pac-gommes.
		SDLRect[] positionsImagesPacGomme = new SDLRect[terrain.obtenirNombreDePacGommes()];
		
		for(int index = 0; index < terrain.obtenirNombreDePacGommes(); index++)
		{
			positionsImagesPacGomme[index] = new SDLRect(terrain.obtenirPacGommes()[index].obtenirAbscisse(), terrain.obtenirPacGommes()[index].obtenirOrdonnee());
			
			if(terrain.obtenirPacGommes()[index].obtenirSuperPacGomme())
			{
				imageSuperPacGomme.blitSurface(ecran, positionsImagesPacGomme[index]);
			}
			
			else
			{
				imagePetitePacGomme.blitSurface(ecran, positionsImagesPacGomme[index]);
			}
		}
		
		SDLEvent.enableKeyRepeat(5, 5);
		SDLEvent event;
		
		SDLMixer.playMusic(musique, 0);
		SDLSurface fleche=SDLImage.load("Sprite/fleche.jpg");
		fleche.blitSurface(ecran, new SDLRect(400, 550));
		// Variable de boucle
		boolean execution = true;
		
		// Variables de temps et de mouvement
		int compteurBouclesTemporelles = 0;
		int boucleTemporelle = 0;
		int maximumBoucleTemporelle = 2500; // Determine la vitesse d'execution du jeu (0 pour obtenir une vitesse maximale).
		int boucleDeMouvements = 0;
		int maximumBoucleDeMouvements = 30;
		int periodeClignotementSuperPacGomme = 60;
		boolean ouvert = true;
		boolean fuitePossible = true;
		boolean gagne = false;
		boolean perdu = false;
		
		pacman.definirMort(true);
		
		SDLTrueTypeFont fpoint = SDLTTF.openFont("Font/game_over.ttf", 60);
		
		String point="Score: 0";
		SDLSurface score = fpoint.renderTextSolid(point , new SDLColor(255, 242, 0));
		
		SDLRect posscro = new SDLRect(10,540);
		
		score.blitSurface(ecran, posscro);
		
		String pointJ2="Score: 0";
		SDLSurface scoreJ2 = fpoint.renderTextSolid(point , new SDLColor(255, 242, 0));
		
		SDLRect posscroJ2 = new SDLRect(210,540);
		
		if(Parametres.MULTIJOUEUR)
		{
			scoreJ2.blitSurface(ecran, posscroJ2);
		}
		
		while (execution)
		{
			// On utilise pollEvent() comme gestionnaire d'evenements
			event = SDLEvent.pollEvent();

			// Si un evenement de type SDLEvent est detecte faire
			if (event instanceof SDLEvent ) {
				switch (event.getType()) {
				case SDLEvent.SDL_QUIT:     execution = false;    break;
				}
			}
			if(event instanceof SDLMouseButtonEvent){
            	SDLMouseButtonEvent eventM = (SDLMouseButtonEvent) event;
            	
            	if((eventM.getX()>=425 ) && (eventM.getX()<450)){
            		if((eventM.getY()>=550 ) && (eventM.getY()<570))
            			pacman.definirProchaineDirection(Direction.HAUT);
            		if((eventM.getY()>=605 ) && (eventM.getY()<625))
            			pacman.definirProchaineDirection(Direction.BAS);
       
            	}
            	else if((eventM.getY()>=575 ) && (eventM.getY()<600)){
            		if((eventM.getX()>=400 ) && (eventM.getX()<425))
            			pacman.definirProchaineDirection(Direction.GAUCHE);
            		if((eventM.getX()>=450 ) && (eventM.getX()<475))
            			pacman.definirProchaineDirection(Direction.DROITE);
            	}
            }

			// Si un evenement de type SDLKeyboardEvent est detecte faire
			if (event instanceof SDLKeyboardEvent ) {
				SDLKeyboardEvent eventK = (SDLKeyboardEvent) event;

				switch (eventK.getSym()) {

				case SDLKey.SDLK_ESCAPE:    execution = false;        break;

				case SDLKey.SDLK_UP  :
					pacman.definirProchaineDirection(Direction.HAUT);
					break;
					
				case SDLKey.SDLK_w  :
					if(Parametres.MULTIJOUEUR)
						pacmanJ2.definirProchaineDirection(Direction.HAUT);
					break;

				case SDLKey.SDLK_LEFT :
					pacman.definirProchaineDirection(Direction.GAUCHE);
					break;
					
				case SDLKey.SDLK_a :
					if(Parametres.MULTIJOUEUR)
						pacmanJ2.definirProchaineDirection(Direction.GAUCHE);
					break;

				case SDLKey.SDLK_DOWN :
					pacman.definirProchaineDirection(Direction.BAS);
					break;
					
				case SDLKey.SDLK_s :
					if(Parametres.MULTIJOUEUR)
						pacmanJ2.definirProchaineDirection(Direction.BAS);
					break;

				case SDLKey.SDLK_RIGHT :
					pacman.definirProchaineDirection(Direction.DROITE);
					break;
					
				case SDLKey.SDLK_d :
					if(Parametres.MULTIJOUEUR)
						pacmanJ2.definirProchaineDirection(Direction.DROITE);
					break;

				default :
					//System.out.println(eventK +" " );
					break;

				}
			}
			
			if(boucleTemporelle == 0)
			{
				// Mise a jour de l'affichage
				imageVide.blitSurface(ecran, positionImageVidePacman);
				if(Parametres.MULTIJOUEUR)
					imageVide.blitSurface(ecran, positionImageVidePacmanJ2);
				imageVide.blitSurface(ecran, positionImageVideFantome);
				imageVide.blitSurface(ecran, positionImageVideFantome2);
				imageVide.blitSurface(ecran, positionImageVideFantome3);
				imageVide.blitSurface(ecran, positionImageVideFantome4);
				
				for(int index = 0; index < terrain.obtenirNombreDePacGommes(); index++)
				{
					positionsImagesPacGomme[index] = new SDLRect(terrain.obtenirPacGommes()[index].obtenirAbscisse(), terrain.obtenirPacGommes()[index].obtenirOrdonnee());
					
					if(terrain.obtenirPacGommes()[index].obtenirSuperPacGomme())
					{
						if(compteurBouclesTemporelles % periodeClignotementSuperPacGomme < periodeClignotementSuperPacGomme / 2)
						{
							imageVide.blitSurface(ecran, positionsImagesPacGomme[index]);
							imageSuperPacGomme.blitSurface(ecran, positionsImagesPacGomme[index]);
						}
						
						else
						{
							imageVide.blitSurface(ecran, positionsImagesPacGomme[index]);
							imageSuperPacGomme2.blitSurface(ecran, positionsImagesPacGomme[index]);
						}
					}
					
					else
					{
						imagePetitePacGomme.blitSurface(ecran, positionsImagesPacGomme[index]);
					}
				}
				
				if(!pacman.obtenirMort())
				{
					// Affichage de l'orientation du Pacman en fonction de sa direction.
					switch(pacman.obtenirDirection())
					{
						case Direction.GAUCHE:
							
							if(ouvert)
							{
								imagePacmanGauche.blitSurface(ecran, positionImagePacman);
							}
							
							else
							{
								imagePacmanGauche2.blitSurface(ecran, positionImagePacman);
							}
							
							break;
						
						case Direction.DROITE:
							
							if(ouvert)
							{
								imagePacmanDroite.blitSurface(ecran, positionImagePacman);
							}
							
							else
							{
								imagePacmanDroite2.blitSurface(ecran, positionImagePacman);
							}
							
							break;
						
						case Direction.HAUT:
							
							if(ouvert)
							{
								imagePacmanHaut.blitSurface(ecran, positionImagePacman);
							}
							
							else
							{
								imagePacmanHaut2.blitSurface(ecran, positionImagePacman);
							}
							
							break;
						
						case Direction.BAS:
							
							if(ouvert)
							{
								imagePacmanBas.blitSurface(ecran, positionImagePacman);
							}
							
							else
							{
								imagePacmanBas2.blitSurface(ecran, positionImagePacman);
							}
							
							break;
						
						default:
							imagePacman.blitSurface(ecran, positionImagePacman);
							break;
					}
				}
				
				if(Parametres.MULTIJOUEUR)
				{
					if(!pacmanJ2.obtenirMort())
					{
						imagePacmanJ2.blitSurface(ecran, positionImagePacmanJ2);
					}
				}
				
				
				if(pacman.obtenirMort())
				{
					positionImagePacman.setLocation(245, 403);
					pacman.definirCoordonnees(245, 403);
					fantome.definirMort(true);
					fantome2.definirMort(true);
					fantome3.definirMort(true);
					fantome4.definirMort(true);
					fantome.definirDureeMort(100);
					fantome2.definirDureeMort(100);
					fantome3.definirDureeMort(100);
					fantome4.definirDureeMort(100);
				}
				
				if(Parametres.MULTIJOUEUR)
				{
					if(pacmanJ2.obtenirMort())
					{
						positionImagePacmanJ2.setLocation(245, 403);
						pacmanJ2.definirCoordonnees(245, 403);
						fantome.definirMort(true);
						fantome2.definirMort(true);
						fantome3.definirMort(true);
						fantome4.definirMort(true);
						fantome.definirDureeMort(100);
						fantome2.definirDureeMort(100);
						fantome3.definirDureeMort(100);
						fantome4.definirDureeMort(100);
					}
				}
				
				if(fantome.obtenirMort())
				{
					fantome.definirCoordonnees(245, 250);
					
					if(fantome.obtenirDureeMort() == 0)
					{
						positionImageFantome.setLocation(245, 199);
						fantome.definirCoordonnees(245, 199);
					}
				}
				
				if(fantome2.obtenirMort())
				{
					fantome2.definirCoordonnees(245, 250);
					
					if(fantome2.obtenirDureeMort() == 0)
					{
						positionImageFantome2.setLocation(245, 199);
						fantome2.definirCoordonnees(245, 199);
					}
				}
				
				if(fantome3.obtenirMort())
				{
					fantome3.definirCoordonnees(245, 250);
					
					if(fantome3.obtenirDureeMort() == 0)
					{
						positionImageFantome3.setLocation(245, 199);
						fantome3.definirCoordonnees(245, 199);
					}
				}
				
				if(fantome4.obtenirMort())
				{
					fantome4.definirCoordonnees(245, 250);
					
					if(fantome4.obtenirDureeMort() == 0)
					{
						positionImageFantome4.setLocation(245, 199);
						fantome4.definirCoordonnees(245, 199);
					}
				}
				
				if(Parametres.MULTIJOUEUR)
				{
					if(!pacman.obtenirMort() && !pacmanJ2.obtenirMort())
					{
						if(fantome.obtenirProie())
						{
							imageFantomeProie.blitSurface(ecran, positionImageFantome);
						}
						
						else
						{
							imageFantome.blitSurface(ecran, positionImageFantome);
						}
						
						if(fantome2.obtenirProie())
						{
							imageFantomeProie.blitSurface(ecran, positionImageFantome2);
						}
						
						else
						{
							imageFantome2.blitSurface(ecran, positionImageFantome2);
						}
						
						if(fantome3.obtenirProie())
						{
							imageFantomeProie.blitSurface(ecran, positionImageFantome3);
						}
						
						else
						{
							imageFantome3.blitSurface(ecran, positionImageFantome3);
						}
						
						if(fantome4.obtenirProie())
						{
							imageFantomeProie.blitSurface(ecran, positionImageFantome4);
						}
						
						else
						{
							imageFantome4.blitSurface(ecran, positionImageFantome4);
						}
					}
				}
				
				else
				{
					if(!pacman.obtenirMort())
					{
						if(fantome.obtenirProie())
						{
							imageFantomeProie.blitSurface(ecran, positionImageFantome);
						}
						
						else
						{
							imageFantome.blitSurface(ecran, positionImageFantome);
						}
						
						if(fantome2.obtenirProie())
						{
							imageFantomeProie.blitSurface(ecran, positionImageFantome2);
						}
						
						else
						{
							imageFantome2.blitSurface(ecran, positionImageFantome2);
						}
						
						if(fantome3.obtenirProie())
						{
							imageFantomeProie.blitSurface(ecran, positionImageFantome3);
						}
						
						else
						{
							imageFantome3.blitSurface(ecran, positionImageFantome3);
						}
						
						if(fantome4.obtenirProie())
						{
							imageFantomeProie.blitSurface(ecran, positionImageFantome4);
						}
						
						else
						{
							imageFantome4.blitSurface(ecran, positionImageFantome4);
						}
					}
				}
				
				ecran.flip();
				
				pacman.seDeplacer();
				if(Parametres.MULTIJOUEUR)
					pacmanJ2.seDeplacer();
				
				if(fuitePossible)
				{
					fantome.troublerEtHanterPacman(pacman);
					fantome2.troublerEtHanterPacman(pacman);
					fantome3.troublerEtHanterPacman(pacman);
					fantome4.troublerEtHanterPacman(pacman);
					
					fuitePossible = false;
				}
				
				else
				{
					if(!fantome.obtenirProie())
					{
						fantome.troublerEtHanterPacman(pacman);
					}
					
					if(!fantome2.obtenirProie())
					{
						fantome2.troublerEtHanterPacman(pacman);
					}
					
					if(!fantome3.obtenirProie())
					{
						fantome3.troublerEtHanterPacman(pacman);
					}
					
					if(!fantome4.obtenirProie())
					{
						fantome4.troublerEtHanterPacman(pacman);
					}
					
					fuitePossible = true;
				}
				
				// Animation de la machoire de Pacman.
				
				if(boucleDeMouvements == maximumBoucleDeMouvements / 2)
				{
					ouvert = false;
				}
				
				if(boucleDeMouvements == maximumBoucleDeMouvements || !pacman.obtenirEnMouvement())
				{
					ouvert = true;
					boucleDeMouvements = 0;
				}
				
				else
				{
					boucleDeMouvements++;
				}
				
				positionImageVidePacman.setLocation(pacman.obtenirAncienneAbscisse(), pacman.obtenirAncienneOrdonnee());
				if(Parametres.MULTIJOUEUR)
					positionImageVidePacmanJ2.setLocation(pacmanJ2.obtenirAncienneAbscisse(), pacmanJ2.obtenirAncienneOrdonnee());
				positionImageVideFantome.setLocation(fantome.obtenirAncienneAbscisse(), fantome.obtenirAncienneOrdonnee());
				positionImageVideFantome2.setLocation(fantome2.obtenirAncienneAbscisse(), fantome2.obtenirAncienneOrdonnee());
				positionImageVideFantome3.setLocation(fantome3.obtenirAncienneAbscisse(), fantome3.obtenirAncienneOrdonnee());
				positionImageVideFantome4.setLocation(fantome4.obtenirAncienneAbscisse(), fantome4.obtenirAncienneOrdonnee());
				
				positionImagePacman.setLocation(pacman.obtenirAbscisse(), pacman.obtenirOrdonnee());
				if(Parametres.MULTIJOUEUR)
					positionImagePacmanJ2.setLocation(pacmanJ2.obtenirAbscisse(), pacmanJ2.obtenirOrdonnee());
				positionImageFantome.setLocation(fantome.obtenirAbscisse(), fantome.obtenirOrdonnee());
				positionImageFantome2.setLocation(fantome2.obtenirAbscisse(), fantome2.obtenirOrdonnee());
				positionImageFantome3.setLocation(fantome3.obtenirAbscisse(), fantome3.obtenirOrdonnee());
				positionImageFantome4.setLocation(fantome4.obtenirAbscisse(), fantome4.obtenirOrdonnee());
				
				if(pacman.obtenirRestaurationPacGomme() && SDLMixer.playing(1) == 0)
				{
					if(pacman.obtenirRestaurationSuperPacGomme())
					{
						SDLMixer.playChannel(1, sonRestaurationPacGomme, 0);
					}
					
					else
					{
						SDLMixer.playChannelTimed(1, sonRestaurationPacGomme, 0, 100);
					}
				}
				
				if(pacman.obtenirRestaurationPacGomme())
				{
					
					
					score= fpoint.renderTextSolid(point ,new SDLColor(0, 0, 0));
					score.blitSurface(ecran, posscro);
					point="Score : " + pacman.obtenirScore();
					score= fpoint.renderTextSolid(point, new SDLColor(255, 242, 0));
					score.blitSurface(ecran, posscro);
				}
				
				if(Parametres.MULTIJOUEUR)
				{
					if(pacmanJ2.obtenirRestaurationPacGomme() && SDLMixer.playing(1) == 0)
					{
						if(pacmanJ2.obtenirRestaurationSuperPacGomme())
						{
							SDLMixer.playChannel(1, sonRestaurationPacGomme, 0);
						}
						
						else
						{
							SDLMixer.playChannelTimed(1, sonRestaurationPacGomme, 0, 100);
						}
					}
					
					if(pacmanJ2.obtenirRestaurationPacGomme())
					{
						scoreJ2= fpoint.renderTextSolid(pointJ2 ,new SDLColor(0, 0, 0));
						scoreJ2.blitSurface(ecran, posscroJ2);
						pointJ2="Score : " + pacmanJ2.obtenirScore();
						scoreJ2= fpoint.renderTextSolid(pointJ2, new SDLColor(255, 242, 0));
						scoreJ2.blitSurface(ecran, posscroJ2);
					}
				}
				
				if(terrain.obtenirNombreDePacGommes() == 0)
				{
					System.out.println("Gagne !");
					execution = false;
					gagne = true;
				}
				
				if(pacman.obtenirNombreDeVies() == 0)
				{
					System.out.println("Perdu !");
					execution = false;
					perdu = true;
				}
			}
			
			if(boucleTemporelle == maximumBoucleTemporelle)
			{
				boucleTemporelle = 0;
				compteurBouclesTemporelles++;
			}
			
			else
			{
				boucleTemporelle++;
			}
		}
		
		SDLMixer.freeChunk(sonRestaurationPacGomme);
		SDLMixer.freeMusic(musique);
		imageFond.freeSurface();
		imageVide.freeSurface();
		imagePacman.freeSurface();
		fleche.freeSurface();
		imagePetitePacGomme.freeSurface();
		imageSuperPacGomme.freeSurface();
		imageSuperPacGomme2.freeSurface();
		imagePacmanGauche.freeSurface();
		imagePacmanDroite.freeSurface();
		imagePacmanHaut.freeSurface();
		imagePacmanBas.freeSurface();
		imagePacmanGauche2.freeSurface();
		imagePacmanDroite2.freeSurface();
		imagePacmanHaut2.freeSurface();
		imagePacmanBas2.freeSurface();
		imageFantome.freeSurface();
		imageFantome2.freeSurface();
		imageFantome3.freeSurface();
		imageFantome4.freeSurface();
		imageFantomeProie.freeSurface();
		imageVie.freeSurface();
		imagePacmanJ2.freeSurface();
		imageVieJ2.freeSurface();
		ecran.freeSurface();
		SDLMain.quit();
		
		if(gagne)
		{
			new Gagner();
		}
		
		if(perdu)
		{
			new Perdre();
		}
	}
}





