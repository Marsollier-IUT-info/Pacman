package pacman;

import pacman.modele.Chemin;
import pacman.modele.PacGomme;
import pacman.modele.Teleporteur;
import pacman.modele.Terrain;

public final class GenerationTerrain
{
	private final static int ABSCISSE = 16;
	private final static int ORDONNEE = 12;
	private final static int INTERVALLE = 17;
	
	private GenerationTerrain(){}
	
	private static void devenirSuperPacGomme(PacGomme pacGomme)
	{
		pacGomme.definirSuperPacGomme(true);
		pacGomme.definirScore(100);
		pacGomme.definirDureeBonus(500);
	}
	
	public static Terrain genererTerrain()
	{
		Terrain terrain = new Terrain();
		
		// Ajout des chemins et pac-gommes.
		// Ajout des chemins horizontaux.
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE, INTERVALLE * 11, true, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 15, ORDONNEE  + INTERVALLE, INTERVALLE * 11, true, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE * 5, INTERVALLE * 25, true, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE * 8, INTERVALLE * 5, true, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 9, ORDONNEE + INTERVALLE * 8, INTERVALLE * 3, true, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 15, ORDONNEE + INTERVALLE * 8, INTERVALLE * 3, true, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 21, ORDONNEE + INTERVALLE * 8, INTERVALLE * 5, true, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 9, ORDONNEE + INTERVALLE * 11, INTERVALLE * 9, true));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE, ORDONNEE + INTERVALLE * 14, INTERVALLE * 9, true));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 18, ORDONNEE + INTERVALLE * 14, INTERVALLE * 9, true));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 9, ORDONNEE + INTERVALLE * 17, INTERVALLE * 9, true));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE * 20, INTERVALLE * 11, true, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 15, ORDONNEE + INTERVALLE * 20, INTERVALLE * 11, true, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE * 23, INTERVALLE * 2, true, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 6, ORDONNEE + INTERVALLE * 23, INTERVALLE * 15, true, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 24, ORDONNEE + INTERVALLE * 23, INTERVALLE *2, true, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE * 26, INTERVALLE * 5, true, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 9, ORDONNEE + INTERVALLE * 26, INTERVALLE * 3, true, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 15, ORDONNEE + INTERVALLE * 26, INTERVALLE * 3, true, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 21, ORDONNEE + INTERVALLE * 26, INTERVALLE * 5, true, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE * 29, INTERVALLE * 25, true, INTERVALLE));
		
		// Ajout des chemins verticaux.
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE, INTERVALLE * 7, false, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE * 20, INTERVALLE * 3, false, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE * 26, INTERVALLE * 3, false, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 6, ORDONNEE + INTERVALLE, INTERVALLE * 25, false, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 3, ORDONNEE + INTERVALLE * 23, INTERVALLE * 3, false, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 9, ORDONNEE + INTERVALLE * 5, INTERVALLE * 3, false, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 9, ORDONNEE + INTERVALLE * 11, INTERVALLE * 9, false));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 9, ORDONNEE + INTERVALLE * 23, INTERVALLE * 3, false, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 12, ORDONNEE + INTERVALLE, INTERVALLE * 4, false, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 12, ORDONNEE + INTERVALLE * 8, INTERVALLE * 3, false));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 12, ORDONNEE + INTERVALLE * 20, INTERVALLE * 3, false, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 12, ORDONNEE + INTERVALLE * 26, INTERVALLE * 3, false, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 15, ORDONNEE + INTERVALLE, INTERVALLE * 4, false, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 15, ORDONNEE + INTERVALLE * 8, INTERVALLE * 3, false));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 15, ORDONNEE + INTERVALLE * 20, INTERVALLE * 3, false, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 15, ORDONNEE + INTERVALLE * 26, INTERVALLE * 3, false, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 18, ORDONNEE + INTERVALLE * 5, INTERVALLE * 3, false, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 18, ORDONNEE + INTERVALLE * 11, INTERVALLE * 9, false));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 18, ORDONNEE + INTERVALLE * 23, INTERVALLE * 3, false, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 21, ORDONNEE + INTERVALLE, INTERVALLE * 25, false, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 24, ORDONNEE + INTERVALLE * 23, INTERVALLE * 3, false, INTERVALLE));
		
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 26, ORDONNEE + INTERVALLE, INTERVALLE * 7, false, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 26, ORDONNEE + INTERVALLE * 20, INTERVALLE * 3, false, INTERVALLE));
		terrain.ajouterChemin(new Chemin(terrain, ABSCISSE + INTERVALLE * 26, ORDONNEE + INTERVALLE * 26, INTERVALLE * 3, false, INTERVALLE));
		
		// Transformation en super pac-gommes.
		devenirSuperPacGomme(terrain.obtenirPacGomme(ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE * 3));
		devenirSuperPacGomme(terrain.obtenirPacGomme(ABSCISSE + INTERVALLE * 26, ORDONNEE + INTERVALLE * 3));
		devenirSuperPacGomme(terrain.obtenirPacGomme(ABSCISSE + INTERVALLE, ORDONNEE + INTERVALLE * 23));
		devenirSuperPacGomme(terrain.obtenirPacGomme(ABSCISSE + INTERVALLE * 26, ORDONNEE + INTERVALLE * 23));
		
		// Ajout des teleporteurs.
		terrain.ajouterTeleporteur(new Teleporteur(terrain, ABSCISSE, ORDONNEE + INTERVALLE * 14, ABSCISSE + INTERVALLE * 27, ORDONNEE + INTERVALLE * 14));
		terrain.ajouterTeleporteur(new Teleporteur(terrain, ABSCISSE + INTERVALLE * 27, ORDONNEE + INTERVALLE * 14, ABSCISSE, ORDONNEE + INTERVALLE * 14));
		
		return terrain;
	}
}
