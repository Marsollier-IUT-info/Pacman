package pacman;

import sdljava.SDLMain;
import sdljava.SDLException;
import sdljava.video.*;
import sdljava.x.swig.SWIGTYPE_p__SDL_Joystick;
import sdljava.event.SDLEvent;
import sdljava.event.SDLKeyboardEvent;
import sdljava.event.SDLKey;
import sdljava.event.SDLMouseButtonEvent;
import sdljava.event.SDLMouseMotionEvent;
import sdljava.image.SDLImage;
import sdljava.joystick.*;
import sdljava.audio.*;
import sdljava.mixer.*;
import sdljava.ttf.*;



public class Menu_nbJoueur {

	//public Menu_nbJoueur(SDLSurface screen){
	public Menu_nbJoueur() throws SDLException{

		// creation de la fenettre de l'application 
		SDLMain.init(SDLMain.SDL_INIT_VIDEO  | SDLMain.SDL_INIT_AUDIO | SDLMain.SDL_INIT_JOYSTICK);
		SDLSurface screen = SDLVideo.setVideoMode(525, 625, 32, SDLVideo.SDL_DOUBLEBUF | SDLVideo.SDL_HWSURFACE);
		SDLVideo.wmSetCaption("Pacman By Axel, Thomas et Adrien Version 0.1", null);




		SDLTTF.init();
		SDLTrueTypeFont monFont = SDLTTF.openFont("Font/game_over.ttf", 60);
		SDLTrueTypeFont monFontT = SDLTTF.openFont("Font/PAC-FONT.ttf", 24);

		SDLSurface titre = monFontT.renderTextSolid("Nombre de joueur : ", new SDLColor(255, 242, 0));

		//SDLSurface classique = monFont.renderTextShaded("1", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
		SDLSurface mode = monFont.renderTextShaded("2", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
		SDLSurface option = monFont.renderTextShaded("3", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
		SDLSurface quit = monFont.renderTextShaded("4", new SDLColor(0, 255, 0),new SDLColor(143,143,143));

		SDLRect posTitre = new SDLRect(75,50);
		titre.blitSurface(screen,posTitre);
		System.out.println(titre.getHeight());
		System.out.println(titre.getWidth());


		SDLRect m1 = new SDLRect(250, 200);
		///classique.blitSurface(screen , m1);
		//System.out.println(classique.getHeight());
		//System.out.println(classique.getWidth());

		SDLRect m2 = new SDLRect(250, 300);
		mode.blitSurface(screen , m2);
		System.out.println(mode.getHeight());
		System.out.println(mode.getWidth());
		
		SDLRect m3 = new SDLRect(250, 400);
		option.blitSurface(screen , m3);
		System.out.println(quit.getHeight());
		System.out.println(quit.getWidth());
		
		SDLRect m4 = new SDLRect(250, 500);
		quit.blitSurface(screen , m4);
		System.out.println(quit.getHeight());
		System.out.println(quit.getWidth());
		
		

		screen.flip();

		boolean Running = true;

		while (Running) {

			SDLEvent event = SDLEvent.waitEvent(); 



			if(event.getType() == SDLEvent.SDL_MOUSEBUTTONUP){
				SDLMouseButtonEvent eventM = (SDLMouseButtonEvent) event;
//				if((eventM.getX()>=250 && eventM.getY()>=200) && (eventM.getX()>250 && eventM.getY()<=235) && (eventM.getX()<=260 && eventM.getY()>=200) && (eventM.getX()<=260 && eventM.getY()<=260)){
//					
//					System.out.println("1");
//					
//				}

				if((eventM.getX()>=250 && eventM.getY()>=300) && (eventM.getX()>250 && eventM.getY()<=335) && (eventM.getX()<=263 && eventM.getY()>=300) && (eventM.getX()<=263 && eventM.getY()<=335)){

					System.out.println("2");
				}

//				if((eventM.getX()>=250 && eventM.getY()>=400) && (eventM.getX()>250 && eventM.getY()<=435) && (eventM.getX()<=263 && eventM.getY()>=400) && (eventM.getX()<=263 && eventM.getY()<=435)){
//
//					System.out.println("3");
//				}
				if((eventM.getX()>=250 && eventM.getY()>=500) && (eventM.getX()>250 && eventM.getY()<=535) && (eventM.getX()<=263 && eventM.getY()>=500) && (eventM.getX()<=263 && eventM.getY()<=535)){
					
					System.out.println("4");
				}

			}

			if(event.getType() == SDLEvent.SDL_MOUSEMOTION){
				SDLMouseMotionEvent eventM = (SDLMouseMotionEvent) event;


//				if((eventM.getX()>=250 && eventM.getY()>=200) && (eventM.getX()>250 && eventM.getY()<=235) && (eventM.getX()<=260 && eventM.getY()>=200) && (eventM.getX()<=260 && eventM.getY()<=260)){
//					classique.freeSurface();
//					classique = monFont.renderTextShaded("1", new SDLColor(0, 255, 0),new SDLColor(255,0,255));
//					classique.blitSurface(screen , m1);
//					titre.blitSurface(screen,posTitre);
//					mode.blitSurface(screen , m2);
//					option.blitSurface(screen, m3);
//					quit.blitSurface(screen , m4);
//					screen.flip();
//
//				}

				if((eventM.getX()>=250 && eventM.getY()>=300) && (eventM.getX()>250 && eventM.getY()<=335) && (eventM.getX()<=263 && eventM.getY()>=300) && (eventM.getX()<=263 && eventM.getY()<=335)){
					mode.freeSurface();
					mode = monFont.renderTextShaded("2", new SDLColor(0, 255, 0),new SDLColor(255,0,255));
				//	classique.blitSurface(screen , m1);
					titre.blitSurface(screen,posTitre);
					mode.blitSurface(screen , m2);
					option.blitSurface(screen, m3);
					quit.blitSurface(screen , m4);
					screen.flip();
				}

//				else  if((eventM.getX()>=250 && eventM.getY()>=400) && (eventM.getX()>250 && eventM.getY()<=435) && (eventM.getX()<=263 && eventM.getY()>=400) && (eventM.getX()<=263 && eventM.getY()<=435)){
//					option.freeSurface();
//					option= monFont.renderTextShaded("3", new SDLColor(0, 255, 0),new SDLColor(255,0,255));
//					classique.blitSurface(screen , m1);
//					titre.blitSurface(screen,posTitre);
//					mode.blitSurface(screen , m2);
//					option.blitSurface(screen , m3);
//					quit.blitSurface(screen , m4);
//					screen.flip();
//
//				}
				else  if((eventM.getX()>=250 && eventM.getY()>=500) && (eventM.getX()>250 && eventM.getY()<=535) && (eventM.getX()<=263 && eventM.getY()>=500) && (eventM.getX()<=263 && eventM.getY()<=535)){
					quit.freeSurface();
					quit = monFont.renderTextShaded("4", new SDLColor(0, 255, 0),new SDLColor(255,0,255));
					//classique.blitSurface(screen , m1);
					titre.blitSurface(screen,posTitre);
					mode.blitSurface(screen , m2);
					option.blitSurface(screen, m3);
					quit.blitSurface(screen , m4);
					screen.flip();
					
				}
				else {
				//	classique = monFont.renderTextShaded("1", new SDLColor(0, 255, 0),new SDLColor(143,143,143)); 
					mode = monFont.renderTextShaded("2", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
				//	option = monFont.renderTextShaded("3", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
					quit = monFont.renderTextShaded("4", new SDLColor(0, 255, 0),new SDLColor(143,143,143));
				//	classique.blitSurface(screen , m1);
					titre.blitSurface(screen,posTitre);
					mode.blitSurface(screen , m2);
					option.blitSurface(screen , m3);
					quit.blitSurface(screen , m4);
					screen.flip();
				}
			}



		}
		screen.fillRect(screen.mapRGB(0, 0, 0));

		screen.freeSurface();
		SDLTTF.quit();
		SDLMain.quit();

	}




}
